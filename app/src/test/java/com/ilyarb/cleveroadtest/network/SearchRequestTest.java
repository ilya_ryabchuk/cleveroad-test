package com.ilyarb.cleveroadtest.network;

import android.app.Activity;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.ilyarb.cleveroadtest.BuildConfig;
import com.ilyarb.cleveroadtest.MainActivity_;
import com.ilyarb.cleveroadtest.models.SearchResult;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.shadows.httpclient.FakeHttpLayer;
import org.robolectric.shadows.httpclient.TestHttpResponse;

import java.io.UnsupportedEncodingException;

@org.robolectric.annotation.Config(constants = BuildConfig.class, sdk = 18)
@RunWith(RobolectricGradleTestRunner.class)
public class SearchRequestTest {

	public static final String DEVELOPER_KEY = "AIzaSyB558ya_R_kUKuUCMKG86muobnL9JxRKkU";

	public static final String CX = "000444338591325485829:hgc2jpgnm78";

	public static final String SEARCH_URL =

		"https://www.googleapis.com/customsearch/v1?" +
			"key=" + DEVELOPER_KEY + "&" +
			"cx=" + CX + "&" +
			"searchType=image&" +
			"q=";

	Activity main;
	FakeHttpLayer http;
	
	@Before
	public void setup() {
		http = new FakeHttpLayer();
		http.addPendingHttpResponse(200, "OK");
		http.interceptHttpRequests(false);
		
		main = new MainActivity_();	
	}

	@Test
	public void testGoogleCustomSearchRequest() {
		RequestQueue queue = Volley.newRequestQueue(main);
		SearchRequest request = new SearchRequest(

			SEARCH_URL + "test",

			new Response.Listener<SearchResult>() {
				@Override
				public void onResponse(SearchResult response) {
					http.addHttpResponse(new TestHttpResponse(200, "OK"));
				}
			},

			new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					http.addHttpResponse(new TestHttpResponse(500, "Error"));
				}
			}

		);

		queue.add(request);
	}
	
	private static class SearchRequest extends JsonRequest<SearchResult> {
		
		public SearchRequest(String url, Response.Listener<SearchResult> listener,
		                     Response.ErrorListener errorListener) {
			
			super(Method.GET, url, null, listener, errorListener);
		}
		
		@Override
		protected Response<SearchResult> parseNetworkResponse(NetworkResponse response) {
			try {
				Gson gson = new Gson();
				String json = new String(
					response.data,
					HttpHeaderParser.parseCharset(response.headers));

				return Response.success(
					gson.fromJson(json, SearchResult.class),
					HttpHeaderParser.parseCacheHeaders(response));

			} catch (UnsupportedEncodingException e) {
				return Response.error(new ParseError(e));

			} catch (JsonSyntaxException e) {
				return Response.error(new ParseError(e));
			}
		}
	}
}
