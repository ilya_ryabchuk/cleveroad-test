package com.ilyarb.cleveroadtest;

import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ilyarb.cleveroadtest.views.adapters.TabPagerAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

	@ViewById
	Toolbar toolbar;

	@ViewById(R.id.drawer_layout)
	DrawerLayout mDrawerLayout;

	@ViewById(R.id.navigation_view)
	NavigationView navigationView;

	@ViewById(R.id.view_pager)
	ViewPager viewPager;

	@ViewById(R.id.tab_layout)
	TabLayout tabLayout;

	@AfterViews
	void initializeUI() {

		setSupportActionBar(toolbar);

		final ActionBar actionBar = getSupportActionBar();
		final TabPagerAdapter adapter = new TabPagerAdapter(getSupportFragmentManager());

		actionBar.setHomeAsUpIndicator(R.mipmap.menu);
		actionBar.setDisplayHomeAsUpEnabled(true);

		// setting navigation drawer items handler
		navigationView.setNavigationItemSelectedListener(onItemSelected);

		// setting tabs
		viewPager.setAdapter(adapter);
		tabLayout.setupWithViewPager(viewPager);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		switch (id) {
			// open navigation drawer
			case android.R.id.home:
				mDrawerLayout.openDrawer(GravityCompat.START);
				return true;

			case R.id.action_settings:
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private NavigationView.OnNavigationItemSelectedListener onItemSelected =
		new NavigationView.OnNavigationItemSelectedListener() {
			@Override
			public boolean onNavigationItemSelected(MenuItem menuItem) {
				switch (menuItem.getItemId()) {

					case R.id.navigation_item_main:
						menuItem.setChecked(true);
						mDrawerLayout.closeDrawers();

						return true;

					case R.id.navigation_item_exit:
						finish();
						return true;
				}

				return false;
			}
		};
}
