package com.ilyarb.cleveroadtest;

public class Config {

	private static final String DEVELOPER_KEY = "AIzaSyB558ya_R_kUKuUCMKG86muobnL9JxRKkU";

	private static final String CX = "000444338591325485829:hgc2jpgnm78";

	private static final int RESULTS_COUNT = 10;

	public static final String SEARCH_URL =

		"https://www.googleapis.com/customsearch/v1?" +
		"key=" + DEVELOPER_KEY + "&" +
		// custom search id
		"cx=" + CX + "&" +
		// search images
		"searchType=image&" +
		// retrieve 10 results
		"num=" + RESULTS_COUNT + "&" +
		// user query
		"q=";

}
