package com.ilyarb.cleveroadtest.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ilyarb.cleveroadtest.models.Image;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String TAG = "DATABASE HELPER";

	private static final int DATABASE_VERSION = 4;

	private static final String DATABASE_NAME = "imagedb";

	private static final String TABLE_IMAGES = "images";

	// Table schema
	private static final String KEY_ID = "id";
	private static final String KEY_IMAGE = "image";
	private static final String KEY_TITLE = "title";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		final String CREATE_IMAGE_TABLE =

			"CREATE TABLE " + TABLE_IMAGES + "("
				+ KEY_ID + " INTEGER PRIMARY KEY, "
				+ KEY_TITLE + " TEXT, "
				+ KEY_IMAGE + " BLOB" + ");";

		db.execSQL(CREATE_IMAGE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGES);
		onCreate(db);
	}

	/* CRUD operations
	====================================== */

	public void addToFavorite(final Image image) {

		final SQLiteDatabase db = getWritableDatabase();
		final ContentValues values = new ContentValues();

		values.put(KEY_IMAGE, image.getImage());
		values.put(KEY_TITLE, image.getTitle());

		db.insert(TABLE_IMAGES, null, values);
		db.close();

	}

	public List<Image> getImages() {

		final SQLiteDatabase db = getReadableDatabase();
		final String query = "SELECT * FROM " + TABLE_IMAGES + ";";
		final Cursor cursor = db.rawQuery(query, null);
		final List<Image> results = new ArrayList<>();

		if (cursor.moveToFirst()) {
			do {

				Image image = new Image();

				image.setId(cursor.getString(0));
				image.setTitle(cursor.getString(1));
				image.setImage(cursor.getBlob(2));

				results.add(image);

			} while (cursor.moveToNext());

			db.close();
			cursor.close();

			return results;
		}

		Log.d(TAG, "RESULTS IS " + results.size());

		return Collections.<Image>emptyList();
	}
}
