package com.ilyarb.cleveroadtest.fragments;

import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.ilyarb.cleveroadtest.R;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.image_detail)
public class ImageDetailFragment extends Fragment {

	@ViewById(R.id.image_detail)
	ImageView imageView;

	@AfterViews
	void initialize() {

		final String IMAGE_LINK = this.getArguments().getString("image_link");

		Picasso.with(getActivity())
			.load(IMAGE_LINK)
			.fit()
			.into(imageView);

	}

	@Click(R.id.back_button)
	void returnBack() {
		getFragmentManager().popBackStack();
	}

}
