package com.ilyarb.cleveroadtest.fragments;

import android.support.v4.app.Fragment;
import android.widget.GridView;

import com.ilyarb.cleveroadtest.R;
import com.ilyarb.cleveroadtest.db.DatabaseHelper;
import com.ilyarb.cleveroadtest.models.Image;
import com.ilyarb.cleveroadtest.views.adapters.FavoriteImageAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;

@EFragment(R.layout.favorites_fragment)
public class FavoritesFragment extends Fragment {

	private static final String TAG = "FavoritesFragment";

	@ViewById(R.id.grid_view)
	GridView gridView;

	private FavoriteImageAdapter adapter;

	private ArrayList<Image> results;

	private DatabaseHelper db;

	@AfterViews
	void initialize() {

		db = new DatabaseHelper(getActivity());
		results = new ArrayList<>(db.getImages());

		if (!results.isEmpty()) {
			adapter = new FavoriteImageAdapter(getActivity(), R.layout.favorite_image_item, results);

		} else {
			adapter = new FavoriteImageAdapter(getActivity(), R.layout.favorite_image_item,
				Collections.<Image>emptyList());
		}

		gridView.setAdapter(adapter);
	}

	public void updateFragment() {
		results.clear();
		results.addAll(db.getImages());
		adapter = new FavoriteImageAdapter(getActivity(), R.layout.favorite_image_item, results);
		adapter.notifyDataSetChanged();
	}

}
