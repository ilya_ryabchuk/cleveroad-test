package com.ilyarb.cleveroadtest.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.ilyarb.cleveroadtest.Config;
import com.ilyarb.cleveroadtest.R;
import com.ilyarb.cleveroadtest.models.Item;
import com.ilyarb.cleveroadtest.models.SearchResult;
import com.ilyarb.cleveroadtest.network.SearchResultLoader;
import com.ilyarb.cleveroadtest.views.adapters.SearchResultAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;

@EFragment(R.layout.search_fragment)
public class SearchFragment extends Fragment implements LoaderManager.LoaderCallbacks<SearchResult>,
		SearchView.OnQueryTextListener {

	@ViewById(R.id.search_results)
	RecyclerView recyclerSearchView;

	@ViewById(R.id.user_hint)
	LinearLayout layout;

	private LinearLayoutManager lm;
	private RecyclerView.Adapter adapter;

	private int nextPage;
	private String currentQuery;

	@AfterViews
	void initialize() {

		lm = new LinearLayoutManager(getActivity());

		recyclerSearchView.setHasFixedSize(true);
		recyclerSearchView.setItemAnimator(new DefaultItemAnimator());
		recyclerSearchView.setLayoutManager(lm);
		recyclerSearchView.addOnScrollListener(onListEnded);

		setHasOptionsMenu(true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		final SearchManager manager =
			(SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

		final SearchView searchView =
			(SearchView) menu.findItem(R.id.action_search).getActionView();

		searchView.setSearchableInfo(
			manager.getSearchableInfo(getActivity().getComponentName()));

		searchView.setOnQueryTextListener(this);
	}

	/**
	 * Handling user search when text changes
	 * restarting the loader with new query
	 *
	 * @param newText new search query
	 * @return event handling status
	 */

	@Override
	public boolean onQueryTextChange(String newText) {
		// bundle to pass new search query
		Bundle args = new Bundle();

		if (newText.equals("")) {
			return false;

		} else {
			args.putString("query", newText);
			// save current query state
			//currentQuery = newText;

			getLoaderManager()
				.restartLoader(R.id.search_loader, args, this);

			// handled successful
			return true;
		}
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	@Override
	public Loader<SearchResult> onCreateLoader(int id, Bundle args) {
		// hiding user hint
		layout.setVisibility(View.GONE);

		return new SearchResultLoader(getActivity(), args.getString("query"));
	}

	@Override
	public void onLoaderReset(Loader<SearchResult> loader) {
		final RecyclerView.Adapter adapter =
			new SearchResultAdapter(Collections.<Item>emptyList(), getActivity());

		recyclerSearchView.setAdapter(adapter);
	}

	@Override
	public void onLoadFinished(final Loader<SearchResult> loader, final SearchResult data) {

		// saving next page item to load more
		// in future
		//nextPage = data.getQueries().getNextPage().get(0).getStartIndex();
		//images.addAll(data.getItems());

		final ArrayList<Item> images = new ArrayList<>(data.getItems());

		adapter = new SearchResultAdapter(images, getActivity());
		adapter.notifyDataSetChanged();

		recyclerSearchView.setAdapter(adapter);
	}

	private boolean loading = true;

	int pastVisibleItems,
		visibleItemCount,
		totalItemCount;

	private RecyclerView.OnScrollListener onListEnded =
		new RecyclerView.OnScrollListener() {
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

				visibleItemCount = lm.getChildCount();
				totalItemCount = lm.getItemCount();
				pastVisibleItems = lm.findFirstVisibleItemPosition();

				if (loading) {
					if ( (visibleItemCount + pastVisibleItems) >= totalItemCount ) {
						loading = false;
						// TODO: Fix strange null bug on loadMoreImages();
					}
				}

			}
		};

	private void loadMoreImages() {

		Bundle args = new Bundle();
		// request next page
		final String newQuery =
			Config.SEARCH_URL + currentQuery + "&start=" + nextPage;

		args.putString("query", newQuery);
		// restart loader
		getLoaderManager()
			.restartLoader(R.id.search_loader, args, this);
	}

}
