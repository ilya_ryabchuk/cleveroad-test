package com.ilyarb.cleveroadtest.network;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.Response.ErrorListener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.ilyarb.cleveroadtest.models.SearchResult;

import java.io.UnsupportedEncodingException;

public class SearchRequest extends Request<SearchResult> {

	private final Gson gson = new Gson();
	private final Class<SearchResult> clazz;
	private final Response.Listener<SearchResult> listener;

	/**
	 * Make a GET request and return a parse object from JSON
	 *
	 * @param url URL of the request make
	 * @param clazz Relevant class Object, for Gson's reflection
	 *
	 */

	public SearchRequest(String url, Class<SearchResult> clazz,
	                     Listener<SearchResult> listener, ErrorListener errorListener) {

		super(Method.GET, url, errorListener);

		this.clazz = clazz;
		this.listener = listener;

	}

	@Override
	protected void deliverResponse(SearchResult response) {
		listener.onResponse(response);
	}

	@Override
	protected Response<SearchResult> parseNetworkResponse(NetworkResponse response) {
		try {

			String json = new String(
				response.data,
				HttpHeaderParser.parseCharset(response.headers));

			return Response.success(
				gson.fromJson(json, clazz),
				HttpHeaderParser.parseCacheHeaders(response));

		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));

		} catch (JsonSyntaxException e) {
			return Response.error(new ParseError(e));
		}
	}

}
