package com.ilyarb.cleveroadtest.network;

import android.content.Context;
import android.support.v4.content.Loader;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.ilyarb.cleveroadtest.Config;
import com.ilyarb.cleveroadtest.models.SearchResult;

public class SearchResultLoader extends Loader<SearchResult> {

	private SearchResult cachedData;

	private RequestQueue queue;

	private String query;

	// tag using for volley to cancel requests
	public static final String TAG = "searchloader";

	public SearchResultLoader(Context context, String query) {
		super(context);
		this.query = query;

		// building a request queue
		final Network network = new BasicNetwork(new HurlStack());
		final Cache cache = new DiskBasedCache(

			getContext().getExternalCacheDir(),
			1024 * 1024 // 1MB cap

		);

		queue = new RequestQueue(cache, network);
	}

	@Override
	public void deliverResult(SearchResult data) {
		cachedData = data;

		if (isStarted()) {
			super.deliverResult(data);
		}
	}

	@Override
	protected void onStartLoading() {
		if (cachedData == null) {
			forceLoad();

		} else {
			super.deliverResult(cachedData);
		}
	}

	@Override
	protected void onForceLoad() {
		// start request
		// cancel any old one requests
		queue.cancelAll(TAG);

		final String URL = Config.SEARCH_URL + query.replace(" ", "+");
		final SearchRequest req = new SearchRequest(URL, SearchResult.class, onSuccess, onError);

		queue.add(req);
		queue.start();
	}

	@Override
	protected void onReset() {
		queue.cancelAll(TAG);
	}

	// Callbacks for search request
	// separated to make code cleaner

	private Response.Listener<SearchResult> onSuccess = new Response.Listener<SearchResult>() {
		@Override
		public void onResponse(final SearchResult response) {
			deliverResult(response);
		}
	};

	private Response.ErrorListener onError = new Response.ErrorListener() {
		@Override
		public void onErrorResponse(VolleyError error) {
			Log.d(TAG, "VOLLEY ERROR: " + error);
		}
	};
}