package com.ilyarb.cleveroadtest.models;

public class NextPageItem {

	private int count;

	private int startIndex;

	public NextPageItem() {}

	public int getCount() {
			return count;
		}

	public int getStartIndex() {
			return startIndex;
		}

}
