package com.ilyarb.cleveroadtest.models;

/* Database image schema */

public class Image {

	private String id;

	private String title;

	private byte[] image;

	public byte[] getImage() {
		return image;
	}

	public String getTitle() {
		return title;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setId(String id) {
		this.id = id;
	}

}
