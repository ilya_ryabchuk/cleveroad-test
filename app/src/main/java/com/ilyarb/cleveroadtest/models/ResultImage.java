package com.ilyarb.cleveroadtest.models;

public class ResultImage {

	private String contextLink;

	private int height;

	private int width;

	private int byteSize;

	private String thumbnailLink;

	private int thumbnailWidth;

	private int ThumbnailHeight;

	public ResultImage() {}

	/* Accessors
	============================== */

	public int getByteSize() {
		return byteSize;
	}

	public String getContextLink() {
		return contextLink;
	}

	public int getHeight() {
		return height;
	}

	public int getThumbnailHeight() {
		return ThumbnailHeight;
	}

	public String getThumbnailLink() {
		return thumbnailLink;
	}

	public int getThumbnailWidth() {
		return thumbnailWidth;
	}

	public int getWidth() {
		return width;
	}
}
