package com.ilyarb.cleveroadtest.models;

// Actual result of the search

public class Item {

	private String title;

	private String link;

	private String cacheID;

	private String formattedUrl;

	private ResultImage image;

	public Item() {}

	public String getCacheID() {
		return cacheID;
	}

	public String getFormattedUrl() {
		return formattedUrl;
	}

	public ResultImage getImage() {
		return image;
	}

	public String getLink() {
		return link;
	}

	public String getTitle() {
		return title;
	}
}
