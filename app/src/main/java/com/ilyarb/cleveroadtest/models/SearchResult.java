package com.ilyarb.cleveroadtest.models;

/*
*
* If search request is successful
* a response will be the following structure
*
*/

import java.util.List;

public class SearchResult {

	// Defining all fields that
	// need to application

	private List<Item> items;

	private Queries queries;

	public List<Item> getItems() {
		return items;
	}

	public SearchResult() {}

	public Queries getQueries() {
		return queries;
	}


}
