package com.ilyarb.cleveroadtest.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ilyarb.cleveroadtest.fragments.FavoritesFragment_;
import com.ilyarb.cleveroadtest.fragments.SearchFragment_;

public class TabPagerAdapter extends FragmentStatePagerAdapter {

	private static final int SEARCH_FRAGMENT = 0;
	private static final int FAVORITES_FRAGMENT = 1;

	private FragmentManager fm;

	public TabPagerAdapter(FragmentManager fm) {
		super(fm);
		this.fm = fm;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
			case SEARCH_FRAGMENT:
				return new SearchFragment_();

			case FAVORITES_FRAGMENT:
				return new FavoritesFragment_();

			default: return null;
		}
	}

	@Override
	public int getCount() {
		return 2;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {

			case SEARCH_FRAGMENT:
				return "Search";

			case FAVORITES_FRAGMENT:
				return "Favorites";

			default: return "";
		}
	}
}
