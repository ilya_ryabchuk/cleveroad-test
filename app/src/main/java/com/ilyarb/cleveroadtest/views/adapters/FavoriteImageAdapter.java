package com.ilyarb.cleveroadtest.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilyarb.cleveroadtest.R;
import com.ilyarb.cleveroadtest.models.Image;

import java.util.ArrayList;
import java.util.List;

public class FavoriteImageAdapter extends ArrayAdapter<Image> {

	private Context context;
	private List<Image> results = new ArrayList<>();
	private int layoutResourceId;

	public FavoriteImageAdapter(Context context, int layoutResourceId, List<Image> data) {
		super(context, layoutResourceId, data);

		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.results = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();

			holder.title = (TextView) row.findViewById(R.id.favorite_image_title);
			holder.image = (ImageView) row.findViewById(R.id.favorite_image);

			row.setTag(holder);

		} else {
			holder = (ViewHolder) row.getTag();
		}

		Image item = results.get(position);
		holder.title.setText(item.getTitle());

		try {

			// get image byte array
			byte[] imageByte = results.get(position).getImage();
			// convert it to bitmap
			Bitmap image = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
			// and add it to UI
			holder.image.setImageBitmap(image);

		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		return row;
	}

	static class ViewHolder {
		TextView title;
		ImageView image;
	}

}
