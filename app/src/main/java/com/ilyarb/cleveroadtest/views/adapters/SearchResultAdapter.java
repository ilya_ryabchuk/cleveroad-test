package com.ilyarb.cleveroadtest.views.adapters;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilyarb.cleveroadtest.R;
import com.ilyarb.cleveroadtest.db.DatabaseHelper;
import com.ilyarb.cleveroadtest.fragments.FavoritesFragment;
import com.ilyarb.cleveroadtest.fragments.ImageDetailFragment_;
import com.ilyarb.cleveroadtest.models.Image;
import com.ilyarb.cleveroadtest.models.Item;
import com.ilyarb.cleveroadtest.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {

	private ArrayList<Item> results;

	private FragmentActivity context;

	public static class ViewHolder extends RecyclerView.ViewHolder
			implements View.OnClickListener {

		private TextView title;
		private ImageView image;
		private CheckBox favorite;

		private ItemClickListener clickListener;

		public ViewHolder(final View item) {
			super(item);

			title = (TextView) item.findViewById(R.id.search_title);
			image = (ImageView) item.findViewById(R.id.search_image);
			favorite = (CheckBox) item.findViewById(R.id.search_favorite);

			item.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			clickListener.onClick(v, getLayoutPosition());
		}

		public void setClickListener(ItemClickListener listener) {
			this.clickListener = listener;
		}
	}

	/**
	 *
	 * @param results array that comes from loader
	 * @param context is needed to load images with Picasso
	 *
	 */

	public SearchResultAdapter(final List<Item> results, final FragmentActivity context) {
		this.results = new ArrayList<>(results);
		this.context = context;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		// create a new view
		View v = LayoutInflater.from(parent.getContext())
			.inflate(R.layout.search_item, parent, false);

		return new ViewHolder(v);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {

		holder.title.setText(results.get(position).getTitle());
		// loading image to image view
		Picasso.with(context)
			.load(results.get(position).getImage().getThumbnailLink())
			.fit()
			.centerCrop()
			.into(holder.image);

		// Set listener, to add as favorite
		holder.favorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					addToFavorite(holder.image, holder.title.getText().toString());
					notifyUser();
				}
			}
		});

		// Setting custom click listener
		// (Recycler view has no default)

		holder.setClickListener(new ItemClickListener() {
			@Override
			public void onClick(View view, int position) {
				viewImageDetails(position);
			}
		});

	}

	@Override
	public int getItemCount() {
		return results.size();
	}

	/**
	 * Converts image to byte array and added
	 * to database with title
	 *
	 * @param imageView image that needs to be added
	 * @param title image title
	 *
	 */

	private void addToFavorite(final ImageView imageView, final String title) {

		final Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
		final ByteArrayOutputStream stream = new ByteArrayOutputStream();

		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		// final image
		final byte[] bitmapData = stream.toByteArray();

		final Image image = new Image();
		image.setTitle(title);
		image.setImage(bitmapData);

		DatabaseHelper helper = new DatabaseHelper(context);
		helper.addToFavorite(image);

		// notify that data has changed
		updateFragment();
	}

	private void notifyUser() {

		Snackbar.make(
			context.findViewById(R.id.coordinator),
			R.string.snackbar_action,
			Snackbar.LENGTH_LONG
		).show();

	}

	/**
	 * Updates fragment after adding
	 * a new image to database
	 *
	 */

	private void updateFragment() {

		// As i used FragmentStatePagerAdapter
		// to access fragment it's need to use view pager and state pager adapter
		final ViewPager vp = (ViewPager) context.findViewById(R.id.view_pager);
		final FragmentStatePagerAdapter a = (FragmentStatePagerAdapter) vp.getAdapter();

		// finding fragment
		final FavoritesFragment f = (FavoritesFragment) a.instantiateItem(vp, 1);
		// updating changes
		f.updateFragment();
	}

	/**
	 * Get clicked item, pass image link as argument
	 * and view it in new fragment on full screen
	 *
	 * @param position clicked item position
	 *
	 */

	private void viewImageDetails(int position) {

		final Bundle args = new Bundle();
		final ImageDetailFragment_ fragment = new ImageDetailFragment_();
		final String imageLink = results.get(position).getImage().getThumbnailLink();

		args.putString("image_link", imageLink);
		fragment.setArguments(args);

		context.getSupportFragmentManager().beginTransaction()
			.replace(R.id.drawer_layout, fragment, "IMAGE_DETAIL")
			.addToBackStack("IMAGE_DETAIL")
			.commit();
	}

}